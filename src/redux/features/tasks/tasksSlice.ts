import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

export const getTasks = createAsyncThunk(
  "Tasks/getTasks",
  async (dispatch, getState) => {
    return await fetch("https://jsonplaceholder.typicode.com/users/11/todos").then(
      (res) => res.json()
    );
  }
);




export const tasksSlice = createSlice({
  name: "task",
  initialState: {
    tasks: [],
    status: '',
  },
  reducers: {
    deleteTask: (state, action) => {
      state.tasks = state.tasks.filter(item => item.id !== action.payload);
    },
    addTask: (state, action) => {
      state.tasks = [...state.tasks, action.payload];
    },
    updateTask: (state, action) => {


      state.tasks = [...state.tasks].map((item) => {
        if (item.id === action.payload.data.id) {
          item.completed = action.payload.flag;
        }
        return item;
      });
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getTasks.pending, (state, action) => {
      state.status = "loading";
    }),
      builder.addCase(getTasks.fulfilled, (state, action) => {
        state.status = "success";
        state.tasks = action.payload;
      }),
      builder.addCase(getTasks.rejected, (state, action) => {
        state.status = "failed";
      })
  },
});

export const { deleteTask, addTask, updateTask } = tasksSlice.actions;
export default tasksSlice.reducer;