import { configureStore } from "@reduxjs/toolkit";
import tasksReducer from "./features/tasks/tasksSlice";
export type RootState = ReturnType<typeof store.getState>
const store = configureStore({
  reducer: {
    tasksR: tasksReducer,
  },
});

//export type RootState = ReturnType<typeof rootReducer>
export type AppDispatch = typeof store.dispatch

export default store;