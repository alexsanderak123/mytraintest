import React from 'react'
import { View, Text, Alert } from 'react-native'
import { LoginButton, AccessToken, LoginManager } from 'react-native-fbsdk';

export default function FacebookLogin({getUserData}) {


   const initUser = (token:string) => {
        fetch('https://graph.facebook.com/v2.5/me?fields=email,first_name,last_name,name,picture.width(720).height(720),friends&access_token=' + token)
            .then((response) => {
                response.json().then((json) => {

                   
                    getUserData(json)
                })
            })
            .catch(() => {
                console.log('ERROR GETTING DATA FROM FACEBOOK')
            })
    }

    return (
        <View>
        <LoginButton
          publishPermissions={['publish_actions']}
          readPermissions={['public_profile']}
          onLoginFinished={
            (error:any, result:any) => {
              if (error) {
                console.log("login has error: " + result.error);
              } else if (result.isCancelled) {
                console.log("login is cancelled.");
              } else {
                AccessToken.getCurrentAccessToken().then(
                  (data:any) => {
                    const { accessToken } = data
                    // console.log(accessToken);
                    initUser(accessToken);


                    
                    console.log(data.accessToken.toString())
                  }
                )
              }
            }
          }
          onLogoutFinished={() => console.log("logout.")}/>
      </View>
    )
}
