import styled from 'styled-components/native';
import { COLORS,  SIZES } from '../../../assets/thems';
import { normalize } from 'react-native-elements';

export const ActionButtonContainer = styled.TouchableOpacity<{ boxShadow?: string, backroundColor?: any, addBackground?: any, padding?: any, borderColor?: any, width?: any, addBorder?: any, borderRadius?: any, alignSelf?: any, marginRight?: any, marginTop?: any, height?: any }>`
  background-color: ${(props) => props.backroundColor !== undefined ? props.backroundColor :
    props.addBackground || props.addBackground === undefined
      ? COLORS.blue2
      : COLORS.white};
  padding: ${(props) => props.padding !== undefined ? props.padding : 0}px ;
  border: ${(props) => props.borderColor !== undefined ? props.borderColor : !props.addBorder || props.addBorder === undefined ? '0px' : '1px'};
   width: ${(props) => props.width !== undefined ? props.width + 'px' : '100%'} ;
   align-self:${(props) => props.alignSelf !== undefined ? props.alignSelf : 'center'} ;
   margin-right: ${(props) => props.marginRight !== undefined ? props.marginRight : 0}px ;
   margin-top: ${(props) => props.marginTop !== undefined ? props.marginTop : 0}px;
   height:${(props) => props.height !== undefined ? props.height : normalize(30)}px ;
  justify-content:center;
  border-radius:${(props) => props.borderRadius !== undefined ? props.borderRadius : normalize(3)}px ;
  box-shadow: ${(props) => props.boxShadow !== undefined ? props.boxShadow : '0px 0px 0px rgb(216, 216, 216)'} ; 
`;
export const ActionButtonText = styled.Text<{ fontSize?: number, textColor?: string, addBackground?: boolean, width?: number, borderRadius?: number }>`
  text-align: center;
  font-size:  ${(props) => props.fontSize !== undefined ? props.fontSize : SIZES.button}px ;
  color: ${(props) => props.textColor !== undefined ?
    props.addBackground || props.addBackground === undefined
      ? COLORS.white
      : props.textColor :
    props.addBackground || props.addBackground === undefined
      ? COLORS.white
      : COLORS.black};
   width: ${(props) => props.width !== undefined ? props.width + 'px' : '100%'} ;
   border-radius:${(props) => props.borderRadius !== undefined ? props.borderRadius : 0}px ;
   align-self:center;
   text-transform: uppercase;
   
`;
