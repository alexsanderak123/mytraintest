import React from 'react';
import { ActionButtonContainer, ActionButtonText } from './style';
import { debounce } from 'lodash';


interface Props {
  _onPress?: any;
  addBackground?: boolean;
  addBorder?: boolean;
  text: string;
  textColor?: string;
  borderColor?: string;
  width?: number;
  backroundColor?: string;
  padding?: number;
  disabled?: boolean;
  fontSize?: number;
  alignSelf?: string;
  marginRight?: number;
  marginTop?: number;
  height?: number;
  boxShadow?: string;
  borderRadius?: number;
  containerStyle?: any;
}

const ActionButton: React.FC<Props> = (props) => {
  const { _onPress, text, disabled = false, containerStyle = {} } = props;
  let onPress = debounce(_onPress, 300, { leading: true, trailing: false });
  return (
    <ActionButtonContainer style={containerStyle} disabled={disabled} onPress={onPress} {...props}>
      <ActionButtonText {...props}>{text}</ActionButtonText>
    </ActionButtonContainer>
  );
};
export default ActionButton;
