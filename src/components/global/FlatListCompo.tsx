import React from 'react';
import { View, Text, FlatList } from 'react-native';
import { normalize } from 'react-native-elements';
import { SIZES } from 'assets/thems';
import moment from 'moment';

interface PropsType {
  data: any,
  renderSeparator?: any,
  renderItem: any,
  loadMoreData?: any,
  numColumns?: number,
  paddingBottom?: number,
  scrollable?: boolean,
  ListHeaderComponent?: any,
  containerStyle?: any
}
interface RefType {

}
const FlatListCompo = React.forwardRef<RefType, PropsType>((props: any, ref: any) => {

  const { data, renderSeparator, renderItem, loadMoreData, paddingBottom, numColumns, containerStyle, scrollable, ListHeaderComponent } = props;
  return (
    <View style={[{ width: '100%' }, containerStyle]}>
      <FlatList
        listKey={moment().valueOf().toString()}
        data={data}
        contentContainerStyle={{
          flexGrow: 1,
          paddingBottom: paddingBottom,
        }}
        ListHeaderComponent={ListHeaderComponent}
        stickyHeaderIndices={ListHeaderComponent ? [0] : undefined}
        scrollEnabled={scrollable}
        ItemSeparatorComponent={renderSeparator}
        updateCellsBatchingPeriod={60}
        numColumns={numColumns}
        initialNumToRender={6}
        // removeClippedSubviews={true}
        renderItem={renderItem}
        keyExtractor={(item, index) => index.toString()}
        onEndReachedThreshold={0.4}
        style={{height:'100%'}}
        onEndReached={() => {
          if (loadMoreData) {
            loadMoreData(data.length);
          }
        }}
      />
    </View>);
});

export default FlatListCompo;

FlatListCompo.defaultProps = {
  paddingBottom: 0,
  numColumns: 1,
  data: [],
  renderSeparator: null,
  loadMoreData: null,
  scrollable: true,
};