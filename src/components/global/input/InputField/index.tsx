import React from 'react';
import { TouchableOpacity, TextInput, Text } from 'react-native';
import Styles from '../style';

interface Props {
  innerRef?: any;
  label: string;
  val: string;
  onChange: any;
  keyboardType: any;
  containerStyle?: any;
  accessibilityState?: any;
  onSubmitEditing?: any;
  returnKeyType?: any;
  placeholder?: any;
  textStyle?: any,
  lableStyle?: any,
  multiline?: any,
  numberOfLines?: any,
  editable?: any,
  isRequired?: boolean,
  onBlur?: any,
}
const InputField: React.FC<Props> = (props) => {
  const {
    innerRef,
    label,
    val,
    onChange,
    keyboardType,
    containerStyle,
    onSubmitEditing,
    returnKeyType,
    placeholder,
    textStyle,
    lableStyle,
    multiline,
    numberOfLines,
    editable,
    isRequired,
    onBlur,
  } = props;

  return (
    /* Touchable that when the component clicked it gives focus to the text input */
    <TouchableOpacity
      style={[Styles.inputContainer, containerStyle]}
      onPress={() => {
        innerRef.current.focus();
      }}
      activeOpacity={1}
    >
      {!!label && <Text style={[Styles.label, lableStyle]}>{isRequired ? "*" : ''}{label}</Text>}
          <TextInput
            ref={innerRef}
            onChangeText={(text) => onChange(text)}
            onSubmitEditing={onSubmitEditing}
            blurOnSubmit={false}
            onBlur= {onBlur}
            returnKeyType={returnKeyType}
            style={[Styles.value, textStyle]}
            value={`${val}`}
            editable={editable}
            keyboardType={keyboardType}
            clearButtonMode="while-editing"
            placeholder={placeholder}
            multiline={multiline}
            numberOfLines={numberOfLines}
          />
       
    </TouchableOpacity>
  );
};

export default InputField;
InputField.defaultProps = {
  multiline: false,
  numberOfLines: 1,
  editable: true,
};