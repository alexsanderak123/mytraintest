import { StyleSheet } from 'react-native';
import { COLORS, SPACE, SIZES, FONT_FAMILY } from '../../../assets/thems';

const Styles = StyleSheet.create({
  title: {},

  inputContainer: {
    backgroundColor: COLORS.white,
    marginBottom:SPACE.space5,
    marginTop:SPACE.space5,
  },
  label: {
    fontSize: SIZES.p,
    color: 'rgb(80, 80, 80)',
    marginBottom: SPACE.space5,
  },
  value: {
    color: 'black',
    fontFamily:FONT_FAMILY.MontserratMedium,
  },
  
});
export default Styles;
