import React from 'react';
import { View, Text } from 'react-native';
import {
  TITLE, GENERAL_TEXT, GLOBAL_CONTAINER, SPACE
} from 'assets/thems';
import ActionButton from 'components/global/bottons/ActionButton';
import { normalize } from 'react-native-elements';
import { SIZES } from '../../assets/thems';
import { Switch } from 'react-native-elements';

interface Props {
  handleDelete: any;
  handalRowPress: any,
  rowData: any,
  handalUpdate: any
}
const TaskRow: React.FC<Props> = (props) => {
  const { handleDelete, handalRowPress, rowData, handalUpdate } = props;

  const deleteItem = () => {

    handleDelete(rowData.id);
  }
  const updateSwitch = (flag, item) => {
    handalUpdate(flag, item);
  }
  return (
    <GLOBAL_CONTAINER padding={SPACE.space10} flexDirection={'row'} alignItems={'flex-end'} >
      <GLOBAL_CONTAINER width={SIZES.width * 0.5}>
        <TITLE >{rowData.title}</TITLE>
        <GENERAL_TEXT>{rowData.completed.toString()}</GENERAL_TEXT>

      </GLOBAL_CONTAINER>
      <GLOBAL_CONTAINER alignItems = {'center'}  width={SIZES.width * 0.3}>
        <Switch onChange={() => updateSwitch(!rowData.completed, rowData)} value={rowData.completed} color="orange" />
      </GLOBAL_CONTAINER>


      <ActionButton
        text={'delete'}
        addBackground={true}
        addBorder={true}
        padding={SPACE.space5}
        marginTop={SPACE.space5}
        width={normalize(50)}
        height={normalize(30)}
        _onPress={deleteItem}
      />
    </GLOBAL_CONTAINER>
  )
}
export default TaskRow;
TaskRow.defaultProps = {

}