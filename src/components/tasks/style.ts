import styled from 'styled-components/native';


export const ScreenBackground = styled.View`
  z-index: 5;
  height: 100%;
  width: 80%;
  background-color: rgba(0, 0, 0, 0.8);
`;

export const ModalView = styled.View<{ width?: any, alignItems?: any }>`
  padding-top: 10%;
  background-color: white;
  flex-direction: column;
  width: ${(props) => props.width ? props.width : '100%'};
  height: 100%;
  align-self: flex-end;
  align-items:${(props) => props.alignItems ? props.alignItems : 'center'};
`;
export const ModalContainer = styled.View<{ marginTop?: any }>`
  display: flex;
  flex: 100%;
  flex-direction: row-reverse;
  margin-top:${(props) => props.marginTop ? props.marginTop : 0}px; 
  
`;