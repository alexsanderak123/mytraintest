import React, { useState } from 'react';
import { View, Text, Modal, Alert, Pressable, SafeAreaView, TouchableOpacity } from 'react-native';
import { SPACE, EMPTY_TAG_HORIZONATAL, PALETTE, GLOBAL_CONTAINER, COLORS, FONT_FAMILY, SIZES } from '../../assets/thems';
import { ModalView, ModalContainer, ScreenBackground } from 'components/tasks/style';
import { CloseIcon } from 'assets/icons';
import ActionButton from 'components/global/bottons/ActionButton';
import { normalize } from 'react-native-elements';
import InputField from 'components/global/input/InputField';


interface Props {
  modalVisible: boolean,
  closeTaskModal: any,
  handalButtonPress: any,
}
const TaskModal: React.FC<Props> = (props) => {

  const { closeTaskModal, modalVisible, handalButtonPress } = props;
  const [taskTitle, setTaskTitle] = useState('');

  const pressOnButton = () => {


    handalButtonPress(taskTitle)
  }

  return (
    <SafeAreaView>
      <Modal
        animationType="fade"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
        }}
      >
        <ModalContainer >
          <ModalView width={'100%'}  >

            <GLOBAL_CONTAINER padding={SPACE.space10} flexDirection={'column'} >
              <GLOBAL_CONTAINER alignItems={'center'} justifyContent={'space-between'} marginBottom={SPACE.space10} paddingRight={SPACE.space5} paddingLeft={SPACE.space5}>
                <TouchableOpacity onPress={closeTaskModal}><CloseIcon /></TouchableOpacity>
                <Text style={[PALETTE.titleText]} >הכנס משימות</Text>
                <EMPTY_TAG_HORIZONATAL />
              </GLOBAL_CONTAINER>

              <GLOBAL_CONTAINER alignItems = {'center'} marginBottom = {SPACE.space20}> 
                <InputField
                  {...props}
                  //innerRef={ref}
                  label={'פרטי משימה'}
                  val={taskTitle}
                  isRequired={true}
                  containerStyle={{ backgroundColor: COLORS.white }}
                  onChange={(value: string) => {
                    //inputRef.current.blur();
                    setTaskTitle(value)

                  }}
                  //onBlur={onBlur}
                  keyboardType={'string'}
                  placeholder={''}
                  editable={true}
                  textStyle={{
                    fontFamily: FONT_FAMILY.MontserratBlack,
                    fontSize: SIZES.formLableTextColor, alignItems: "center", backgroundColor: COLORS.greyishBrown, paddingTop: SPACE.space10,
                    height: normalize(80), padding: '3%', paddingLeft: SPACE.space10, color: COLORS.white,
                    width: SIZES.width * 0.75
                  }}
                  //lableStyle={{}}
                  multiline={false}
                  numberOfLines={5}
                />
            
              </GLOBAL_CONTAINER>
              <ActionButton
                text={'Save'}
                addBackground={true}
                addBorder={true}
                padding={SPACE.space5}
                marginTop={SPACE.space5}
                width={normalize(185)}
                height={normalize(30)}
                _onPress={pressOnButton}
              />

            </GLOBAL_CONTAINER>
          </ModalView>
          <ScreenBackground />
        </ModalContainer>

      </Modal>

    </SafeAreaView>
  )
}
export default TaskModal;
TaskModal.defaultProps = {

}