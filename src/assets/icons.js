import { COLORS, FONT_WEIGHTS } from 'assets/thems';
import React from 'react';
import { Image, View } from 'react-native';
import { normalize } from 'react-native-elements';
import Fontawesome5Icon from 'react-native-vector-icons/FontAwesome5';
import IoniconsIcon from 'react-native-vector-icons/Ionicons';
import styled from 'styled-components/native';
import Icon_Close from './icons/svg/Icon_Close.svg';


export const CloseIcon = (props) => (
  <Icon_Close
    width={props.width ? props.width : normalize(15)}
    height={props.width ? props.width : normalize(15)}
    fill={props.color ? props.color : COLORS.greyishBrown}
  // stroke={props.stroke ? props.stroke :COLORS.greyishBrown}
  />
);