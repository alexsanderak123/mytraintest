import { Dimensions, StyleSheet } from 'react-native';
const { width, height } = Dimensions.get('window');
import { normalize } from 'react-native-elements';
import styled, { css } from 'styled-components/native';

export const COLORS = {
  noColor: 'transparent',
  primary: '#00996D',
  secondary: '#606d87',
  background: '#FFFFFF',
  shadow: 'rgba(0, 0, 0, 0.5)',
  titleColor: 'black',
  regularAppTextColor: '#606d87',
  white: '#ffffff',
  black: '#000000',
  blue2: '#223c99',
  header: '#223c99',
  formLableTextColor:'black',
  greyishBrown: 'rgba(74, 74, 74, 0.65)',
  // colors
};
export const SIZES = {
  // global sizes
  base: normalize(8),
  font: normalize(14),
  radius: normalize(12),
  // font sizes
  largeTitle: normalize(50),
  test: normalize(20),
  h1: normalize(20),
  h2: normalize(15),
  h3: normalize(13),
  h4: normalize(11),
  h5: normalize(9),
  p: normalize(10),
  buttonTitle: normalize(13),
  button: normalize(10),
  formLableTextColor:normalize(10),

  // app dimensions
  width,
  height,
};

export const FONT_FAMILY = {
  font: 'W02Lt',
  avenirHeavy: 'Avenir-Heavy',
  avenirBook: 'Avenir-Book',
  MontserratMedium: 'Montserrat-Medium',
  MontserratBlack: 'Montserrat-Black',
  MontserratBlackItalic: 'Montserrat-BlackItalic',
  MontserratBold: 'Montserrat-Bold',
  MontserratBoldItalic: 'Montserrat-BoldItalic',
  MontserratExtraBold: 'Montserrat-ExtraBold',
  MontserratExtraBoldItalic: 'Montserrat-ExtraBoldItalic',
  MontserratExtraLight: 'Montserrat-ExtraLight',
  MontserratExtraLightItalic: 'Montserrat-ExtraLightItalic',
  MontserratItalic: 'Montserrat-Italic',
  MontserratLight: 'Montserrat-Light',
  MontserratLightItalic: 'Montserrat-LightItalic',
  MontserratMediumItalic: 'Montserrat-MediumItalic',
  MontserratRegular: 'Montserrat-Regular',
  MontserratSemiBold: 'Montserrat-SemiBold',
  MontserratSemiBoldItalic: 'Montserrat-SemiBoldItalic',
  MontserratThin: 'Montserrat-Thin',
  MontserratThinItalic: 'Montserrat-ThinItalic',
};
export const FONT_WEIGHTS = {
  light: '100',
  semilight: '200',
  biglight: '300',
  regular: '400',
  lightbold: '500',
  semibold: '600',
  bold: '700',
};


export const SPACE = {
  space3: normalize(3),
  space5: normalize(5),
  space7: normalize(7),
  space10: normalize(10),
  space15: normalize(15),
  space20: normalize(20),
  space25: normalize(25),
  space30: normalize(30),
  space35: normalize(35),
  space40: normalize(40),
};



export const TITLE = styled.Text<{ opacity?: number, textDecoration?: any, margin?: number, fontWeight?: string, fontSize?: number, color?: string, textAlign?: string, fontFamily?: any, marginLeft?: number, marginRight?: number, marginTop?: number, marginBottom?: number, padding?: number, paddingLeft?: number, paddingRight?: number, paddingTop?: number, paddingBottom?: number, alignItems?: string, textTransform?: string }>`
  font-size: ${(props: any) => (props.fontSize ? props.fontSize : SIZES.h1)}px;
  color: ${(props: any) => (props.color ? props.color : COLORS.titleColor)};
  text-align: ${(props: any) => (props.textAlign ? props.textAlign : 'left')};
  padding:${(props: any) => (props.padding ? props.padding : 0)}px;
  padding-left : ${(props: any) => (props.paddingLeft ? props.paddingLeft : 0)}px;
  padding-right : ${(props: any) => (props.paddingRight ? props.paddingRight : 0)}px;
  padding-top : ${(props: any) => (props.paddingTop ? props.paddingTop : 0)}px;
  padding-bottom : ${(props: any) => (props.paddingBottom ? props.paddingBottom : 0)}px;
  font-weight:${(props: any) => (props.fontWeight ? props.fontWeight : FONT_WEIGHTS.bold)};
  margin:${(props: any) => (props.margin ? props.margin : 0)}px;
  ${(props) => props.marginLeft && css` margin-left: ${(props: any) => (props.marginLeft ? props.marginLeft : 0)}px;`}
  ${(props) => props.marginRight && css` margin-right: ${(props: any) => (props.marginRight ? props.marginRight : 0)}px;`}
  ${(props) => props.marginTop && css`  margin-top: ${(props: any) => (props.marginTop ? props.marginTop : 0)}px;`}
  ${(props) => props.marginBottom && css`  margin-bottom: ${(props: any) => (props.marginBottom ? props.marginBottom : 0)}px;`} 
  font-family:  ${(props: any) => (props.fontFamily ? props.fontFamily : FONT_FAMILY.MontserratRegular)} ;
  text-decoration:${(props: any) => (props.textDecoration ? props.textDecoration : 'none')} ;
  text-decoration-color: ${(props: any) => (props.textDecorationColor ? props.textDecorationColor : COLORS.titleColor)} ;
  opacity: ${(props: any) => (props.opacity ? props.opacity : 1)};
  ${(props) => props.textTransform && css`  text-transform: ${(props: any) => (props.textTransform)}`} 
  `;



export const GENERAL_TEXT = styled.Text<{ opacity?: number, textDecoration?: any, margin?: number, fontWeight?: string, fontSize?: number, color?: string, textAlign?: string, fontFamily?: any, marginLeft?: number, marginRight?: number, marginTop?: number, marginBottom?: number, padding?: number, paddingLeft?: number, paddingRight?: number, paddingTop?: number, paddingBottom?: number, alignItems?: string, textTransform?: string }>`
  font-size: ${(props: any) => (props.fontSize ? props.fontSize : SIZES.p)}px;
  color: ${(props: any) => (props.color ? props.color : COLORS.regularAppTextColor)};
  text-align: ${(props: any) => (props.textAlign ? props.textAlign : 'left')};
  padding:${(props: any) => (props.padding ? props.padding : 0)}px;
  padding-left : ${(props: any) => (props.paddingLeft ? props.paddingLeft : 0)}px;
  padding-right : ${(props: any) => (props.paddingRight ? props.paddingRight : 0)}px;
  padding-top : ${(props: any) => (props.paddingTop ? props.paddingTop : 0)}px;
  padding-bottom : ${(props: any) => (props.paddingBottom ? props.paddingBottom : 0)}px;
  /* font-weight:${(props: any) => (props.fontWeight ? props.fontWeight : FONT_WEIGHTS.regular)}; */
  margin:${(props: any) => (props.margin ? props.margin : 0)}px;
  ${(props) => props.marginLeft && css` margin-left: ${(props: any) => (props.marginLeft ? props.marginLeft : 0)}px;`}
  ${(props) => props.marginRight && css` margin-right: ${(props: any) => (props.marginRight ? props.marginRight : 0)}px;`}
  ${(props) => props.marginTop && css`  margin-top: ${(props: any) => (props.marginTop ? props.marginTop : 0)}px;`}
  ${(props) => props.marginBottom && css`  margin-bottom: ${(props: any) => (props.marginBottom ? props.marginBottom : 0)}px;`} 
  font-family:  ${(props: any) => (props.fontFamily ? props.fontFamily : FONT_FAMILY.MontserratRegular)} ;
  text-decoration:${(props: any) => (props.textDecoration ? props.textDecoration : 'none')} ;
  text-decoration-color: ${(props: any) => (props.textDecorationColor ? props.textDecorationColor : COLORS.regularAppTextColor)} ;
  opacity: ${(props: any) => (props.opacity ? props.opacity : 1)};
  ${(props) => props.textTransform && css`  text-transform: ${(props: any) => (props.textTransform)}`} 
  `;

export const GLOBAL_CONTAINER = styled.View<{ position?: string, backgroundColor?: string, flexDirection?: string, justifyContent?: string, width?: number, alignSelf?: string, margin?: number, marginLeft?: number, marginRight?: number, marginTop?: number, marginBottom?: number, padding?: number, paddingLeft?: number, paddingRight?: number, paddingTop?: number, paddingBottom?: number, alignItems?: string, bottom?: number, height?: number, borderWidth?: number, borderColor?: string }>`
flex-direction:${(props: any) => (props.flexDirection ? props.flexDirection : 'column')};
justify-content: ${(props: any) => (props.justifyContent ? props.justifyContent : 'flex-start')};
align-items: ${(props: any) => (props.alignItems ? props.alignItems : 'stretch')};
width: ${(props: any) => (props.width ? props.width + 'px' : '100%')};
align-self:${(props: any) => (props.alignSelf ? props.alignSelf : 'center')};
margin:${(props: any) => (props.margin ? props.margin : 0)}px;
${(props) => props.marginLeft && css` margin-left: ${(props: any) => (props.marginLeft ? props.marginLeft : 0)}px;`}
${(props) => props.marginRight && css` margin-right: ${(props: any) => (props.marginRight ? props.marginRight : 0)}px;`}
${(props) => props.marginTop && css`  margin-top: ${(props: any) => (props.marginTop ? props.marginTop : 0)}px;`}
${(props) => props.marginBottom && css`  margin-bottom: ${(props: any) => (props.marginBottom ? props.marginBottom : 0)}px;`} 
padding:${(props: any) => (props.padding ? props.padding : 0)}px;
${(props) => props.paddingLeft && css` padding-left: ${(props: any) => (props.paddingLeft ? props.paddingLeft : 0)}px;`}
${(props) => props.paddingRight && css` padding-right: ${(props: any) => (props.paddingRight ? props.paddingRight : 0)}px;`}
${(props) => props.paddingTop && css`  padding-top: ${(props: any) => (props.paddingTop ? props.paddingTop : 0)}px;`}
${(props) => props.paddingBottom && css`  padding-bottom: ${(props: any) => (props.paddingBottom ? props.paddingBottom : 0)}px;`} 
background-color: ${(props: any) => (props.backgroundColor ? props.backgroundColor : 'transparent')};
border-width :  ${(props: any) => (props.borderWidth ? props.borderWidth : 0)}px;
border-color : ${(props: any) => (props.borderColor ? props.borderColor : 'transparent')};
position :${(props: any) => (props.position ? props.position : 'relative')};

`;






export const HORIZONTAL = styled.View<{padding?: number, margin?:number}>`
  flex-direction:row;
  justify-content:space-between;
  ${(props) => props.padding && css` padding: ${(props: any) => (props.padding ? props.padding : 0)}px;`}
  ${(props) => props.margin && css` padding: ${(props: any) => (props.margin ? props.margin : 0)}px;`}

`;

export const VERTICAL = styled.View<{padding?: number, margin?:number}>`
  flex-direction:column;
  justify-content:space-between;
  ${(props) => props.padding && css` padding: ${(props: any) => (props.padding ? props.padding : 0)}px;`}
  ${(props) => props.margin && css` padding: ${(props: any) => (props.margin ? props.margin : 0)}px;`}
`;

export const SCROLL_VIEW = styled.ScrollView`
  flex-direction:column;
  width: ${(props: any) => (props.width ? props.width + 'px' : '100%')};
`;


export const EMPTY_TAG_VERICAL = styled.View<{ height?: number }>`
    height:${(props: any) => (props.height ? props.height : 0)}px;
`;
export const EMPTY_TAG_HORIZONATAL = styled.View<{ width?: number }>`

width:${(props: any) => (props.width ? props.width : 0)}px;
`;



export const PALETTE = StyleSheet.create({
  backgroundStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 32,
    paddingHorizontal: 24,
    flex: 1
  },
  touch: {
    top: normalize(10),
    bottom: normalize(10),
    left: normalize(10),
    right: normalize(10)
  },
  titleText: {
    fontSize: normalize(27),
    fontWeight: 'bold',
    marginTop: normalize(26),
    marginBottom: normalize(33),
  },
  
});