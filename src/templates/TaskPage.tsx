import React, { useRef, useEffect, useState } from 'react';
import { View, Text, SafeAreaView, Modal } from 'react-native';
import TaskRow from 'components/tasks/TaskRow';
import FlatListCompo from 'components/global/FlatListCompo';
import { SPACE, COLORS, SIZES } from 'assets/thems';
import ActionButton from 'components/global/bottons/ActionButton';
import { normalize } from 'react-native-elements';
import { useSelector, useDispatch } from "react-redux";
import { getTasks, deleteTask, addTask, updateTask } from "../redux/features/tasks/tasksSlice";
import TaskModal from 'components/tasks/TaskModal';

interface Props {
  navigation: any,
  route: any,
}
const TaskPage: React.FC<Props> = (props) => {
  const flatlistRef: any = useRef();
  const dispatch = useDispatch();
  const { tasks } = useSelector((state: any) => state.tasksR);

  const [taskModalData, setTaskModalData] = useState({ visibility: false, currentData: null })


  //useEffect(() => {
  // dispatch(getTasks());
  //}, [dispatch]);

  const handalRowPress = (item: any) => {

  }
  const loadMoreData = () => {

  }
  const handalUpdate = (flag, data) => {
    dispatch(updateTask({ flag: flag, data: data }));
  }
  const handleDelete = (taskId: string) => {
    dispatch(deleteTask(taskId));

  }
  const renderSeparator = () => (
    <View
      style={{
        backgroundColor: COLORS.black,
        height: 1,
        width: '100%',
        alignSelf: 'center',
      }}
    />
  );
  const renderItem = (currentItem: any) => {
    const { item } = currentItem;
    return <TaskRow handalUpdate={handalUpdate} handleDelete={handleDelete} handalRowPress={handalRowPress} rowData={item}
    />
  };
  const addNewTask = () => {
    setTaskModalData({ ...taskModalData, visibility: true })

  }
  const closeTaskModal = () => {
    setTaskModalData({ ...taskModalData, visibility: false })
  }
  const handalButtonPress = (title: string) => {

    const dataItem = {
      id: tasks.length,
      title: title,
      completed: false
    }

    dispatch(addTask(dataItem));
    closeTaskModal();
  }
  return (
    <SafeAreaView>
      <View style={{ height: SIZES.height * 0.75 }}>
        <FlatListCompo data={tasks} ref={flatlistRef} renderSeparator={renderSeparator}
          numColumns={1} renderItem={renderItem} loadMoreData={loadMoreData}
        />
      </View>

      <ActionButton
        text={'add Tasks'}
        addBackground={true}
        addBorder={true}
        padding={SPACE.space5}
        marginTop={SPACE.space5}
        width={normalize(185)}
        height={normalize(30)}
        _onPress={addNewTask}
      />

      {taskModalData.visibility && <TaskModal handalButtonPress={handalButtonPress} modalVisible={taskModalData.visibility} closeTaskModal={closeTaskModal} />}

    </SafeAreaView>
  )
}
export default TaskPage;
TaskPage.defaultProps = {
}