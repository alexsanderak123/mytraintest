import React, { useEffect, useState, useReducer } from 'react';
import { GoogleSignin, GoogleSigninButton, statusCodes, User } from '@react-native-google-signin/google-signin';
import firebase from 'react-native-firebase';
import {
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';
import FBLoginButton from 'components/FacebookLogin'
import { Avatar, Button, Icon, normalize } from 'react-native-elements';
import { SPACE, VERTICAL, EMPTY_TAG_VERICAL, GLOBAL_CONTAINER } from 'assets/thems';
import {
  PALETTE, TITLE, GENERAL_TEXT,
  HORIZONTAL
} from 'assets/thems';
import ActionButton from 'components/global/bottons/ActionButton';
import { navigateTo } from 'helpers/functions';


interface Props {
  navigation: any,
  route: any,
}
const ACTIONS = {
  UPDATE_LOGIN_AND_USER_DATA: 'updateLoginAndUserData',
  LOGIN_DATA: 'loginData',
}
const initialState = { userInfo: {}, loggedIn: false }

const reducer = (state: any, action: any) => {
  switch (action.type) {
    case ACTIONS.UPDATE_LOGIN_AND_USER_DATA:
      return { ...state, loggedIn: action.loggedIn, userInfo: action.userInfo }
    default:
      return state
  }
}
const LoginPage: React.FC<Props> = (props) => {
  const { navigation } = props;
  const isDarkMode = useColorScheme() === 'dark';
  const [state, localDispatch] = useReducer(reducer, initialState)
  const updateUserData = (item: any) => {
    localDispatch({
      type: ACTIONS.UPDATE_LOGIN_AND_USER_DATA,
      loggedIn: true,
      userInfo: { name: item.name, photo: item.picture.data.url }
    })
  }


  useEffect(() => {
    GoogleSignin.configure({
      webClientId: '597010907107-33dc4btqfbusij48usjcntrnn89inco8.apps.googleusercontent.com'
    });
  }, []);
  async function firebaseGoogleLogin() {
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      // setUserInfo(userInfo)
      // setLoggedIn(true)
      const credential = firebase.auth.GoogleAuthProvider.credential(userInfo.idToken)
      const firebaseUserCredential = await firebase.auth().signInWithCredential(credential);
      console.warn(JSON.stringify(firebaseUserCredential.user.toJSON()));
    } catch (error) {
      console.log(error)
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        console.log("user cancelled the login flow");
      } else if (error.code === statusCodes.IN_PROGRESS) {
        console.log("operation (f.e. sign in) is in progress already");
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        console.log("play services not available or outdated");
      } else {
        console.log("some other error happened");
      }
    }
  }

  const showTaskPage = () => {

    navigateTo(navigation, 'TaskPage');
  }
  return (

    <SafeAreaView style={PALETTE.backgroundStyle}>
      <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
      <TITLE marginBottom={SPACE.space20} >Welcome {state.loggedIn ? state.userInfo.name : "stranger"}!</TITLE>


      {state.loggedIn ?
        <GLOBAL_CONTAINER alignItems={'center'}>
          <Avatar
            size="xlarge"
            rounded
            source={{
              uri:
                `${state.userInfo.photo}`,
            }}
          />
          <EMPTY_TAG_VERICAL height={normalize(15)} />
          <ActionButton
            text={'Show Tasks'}
            addBackground={true}
            addBorder={true}
            padding={SPACE.space5}
            marginTop={SPACE.space5}
            width={normalize(185)}
            height={normalize(30)}
            _onPress={showTaskPage}
          />
        </GLOBAL_CONTAINER> :
        <View >
          <Icon type='material' name="account-circle" size={200} />
          <GENERAL_TEXT marginBottom={SPACE.space10}>Please log in to continue to the awesomeness</GENERAL_TEXT>
        </View>
      }

      <GLOBAL_CONTAINER marginTop={SPACE.space10} alignItems={'center'}>
        <FBLoginButton getUserData={updateUserData} />
        <EMPTY_TAG_VERICAL height={normalize(15)} />
        {false && < GoogleSigninButton
          size={GoogleSigninButton.Size.Wide}
          color={GoogleSigninButton.Color.Dark}
          onPress={() => { firebaseGoogleLogin() }}
        />}
      </GLOBAL_CONTAINER>

    </SafeAreaView >
  )
}
export default LoginPage;

LoginPage.defaultProps = {
}