module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      'module-resolver',
      {
        root: ['./src'],
        extensions: ['.ios.js', '.android.js', '.js', '.ts', '.tsx', '.json'],
        alias: {
          tests: './src/tests/',
          app: './src',
          applicationConstants: './src/constants/applicationConstants',
          components: './src/components',
          templates: './src/templates',
          assets: './src/assets',
          services: './src/services',
          contexts: './src/contexts',
          helpers: './src/helpers',
          action: './src/actions',
          reducers: './src/reducers',
          config: './src/config',
          lib: './src/lib',
        },
      },
    ],
  ],
};
