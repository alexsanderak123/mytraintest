import React from 'react';
import { View, Text, SafeAreaView } from 'react-native';
import LoginPage from './src/templates/LoginPage';
import TaskPage from './src/templates/TaskPage';
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import { Provider } from "react-redux";
import store from "./src/redux/store";
import { COLORS } from './src/assets/thems';

interface Props {
  navigation: any,
  route: any,
}
console.disableYellowBox = true;
const Stack = createStackNavigator();
const App: React.FC<Props> = (props) => {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator
          initialRouteName="LoginPage"
          headerMode="float"
          screenOptions={{
            headerTintColor: 'white',
            gestureEnabled: true,
            gestureDirection: "horizontal",
            headerStyle: { backgroundColor: COLORS.header },
          //  ...TransitionPresets.ModalSlideFromBottomIOS,
            // cardStyleInterpolator:CardStyleInterpolators.forHorizontalIOS,
            // transitionSpec:{
            //   open: config,
            //   close: closeConfig,
            // }
          }}
        >
          <Stack.Screen name="LoginPage" component={LoginPage}
            options={{
              title: 'LoginPage',
              headerShown: true,
              headerTitleAlign: "center",
            }}
          />
          <Stack.Screen name="TaskPage" component={TaskPage}
            options={{
              title: 'TaskPage',
              headerShown: true,
              headerTitleAlign: "center",
            }} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  )
}
export default App;
App.defaultProps = {

}