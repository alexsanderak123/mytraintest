module.exports = {
    preset: 'react-native',
    moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
    setupFilesAfterEnv: ['@testing-library/jest-native/extend-expect'],
    collectCoverage: true,
    setupFiles: ['./jest/jest.setup.js'],
    collectCoverageFrom: ['<rootDir>/src/**/*.{js,jsx,ts,tsx}'],
    coverageThreshold: {
      global: {
        statements: 50,
        branches: 44,
        functions: 35,
        lines: 50,
      },
    },
  };
  